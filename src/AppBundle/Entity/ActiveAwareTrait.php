<?php
namespace AppBundle\Entity;

trait ActiveAwareTrait{

    /**
     * @var bool
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * Enable Set active true
     *
     * @return $this
     */
    public function enable()
    {
        $this->active = true;

        return $this;
    }

    /**
     * Enable Set active true
     *
     * @return $this
     */
    public function disable()
    {
        $this->active = false;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }
}