<?php

namespace AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\CartItem;

/**
 * Cart
 *
 * @ORM\Table(name="cart")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CartRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Cart
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="sub_total", type="float")
     */
    private $sub_total;

    /**
     * @var float
     */
    private $tax_cost = 10;

    /**
     * @var float
     */
    private $shipping_cost = 10;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="date")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CartItem", mappedBy="cart")
     */
    private $cart_items;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\User", inversedBy="cart")
     */
    private $user;


    public function __construct()
    {
        $this->cart_items = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->sub_total + $this->shipping_cost + $this->tax_cost;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->sub_total;
    }

    /**
     * @param float $sub_total
     */
    public function setSubTotal($sub_total)
    {
        $this->sub_total = $sub_total;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Add CartItems
     *
     * @param \AppBundle\Entity\CartItem $cart_item
     * @return Category
     */
    public function addCartItem(CartItem $cart_item)
    {
        $this->cart_items[] = $cart_item;

        return $this;
    }

    /**
     * Remove CartItem
     *
     * @param \AppBundle\Entity\CartItem $cart_item
     */
    public function removeCartItem(CartItem $cart_item)
    {
        $this->cart_items->removeElement($cart_item);
    }

    /**
     * Get cart_items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCartItems()
    {
        return $this->cart_items;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return float
     */
    public function getTaxCost()
    {
        return $this->tax_cost;
    }

    /**
     * @param float $tax_cost
     */
    public function setTaxCost($tax_cost)
    {
        $this->tax_cost = $tax_cost;
    }

    /**
     * @return float
     */
    public function getShippingCost()
    {
        return $this->shipping_cost;
    }

    /**
     * @param float $shipping_cost
     */
    public function setShippingCost($shipping_cost)
    {
        $this->shipping_cost = $shipping_cost;
    }

    /**
     * @ORM\PreUpdate
     */
    public function recalculateSubTotal()
    {
        $price = 0;

        foreach($this->cart_items as $cart_item){
            $price += $cart_item->getPrice();
        }

        $this->sub_total = $price;
    }

}
