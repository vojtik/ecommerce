<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Cart;
use AppBundle\Entity\CartItem;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadCartData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 400;
    }

    public function load(ObjectManager $manager)
    {

        for($i=1;  $i < 10; $i++){

            $cart = new Cart();

            $cart->setUser($this->getReference('user_'.$i));
            $cart->setSubTotal(1488);
            $cart->setUpdatedAt(new \DateTime());

            for($i2=1;  $i2 < 20; $i2++){
                $cartItem = new CartItem();
                $cartItem->setProduct($this->getReference('product_'.rand(1, 10)));

                $cartItem->setPrice($this->getReference('product_'.rand(1, 10))->getPrice());

//                $cart->addCartItem($cart_item);
                $cartItem->setCart($cart);

                $cart->setSubTotal(0);
//                $manager->persist($cart);

                $manager->persist($cartItem);
            }

            $manager->persist($cart);
            $manager->flush();
        }

    }

}