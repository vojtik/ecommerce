<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Category;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadCategorytData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 150;
    }


    public function load(ObjectManager $manager)
    {

        $row = 1;
        if (($handle = fopen(__DIR__.'/category.csv', "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
                $category = new Category();

                $category->setTitle($data[0]);

                if($data[1]) {
                    $category->enable();
                }else{
                    $category->disable();
                }

                $manager->persist($category);
                $manager->flush();

                $this->setReference('category_'.$row , $category);

                $row++;

            }
            fclose($handle);
        }


    }

}