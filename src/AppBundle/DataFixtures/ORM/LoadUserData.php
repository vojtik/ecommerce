<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class LoadUsertData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getOrder()
    {
        return 100;
    }

    public function load(ObjectManager $manager)
    {


        for($i=1;  $i < 20; $i++){

            $user = new User();

            $user->setEmail('user'.$i.'@youfucking.idiot')
                ->setPassword($this->container->get('security.password_encoder')->encodePassword($user,$i.$i.$i));

            if($i < 10){
                $user->setRoles(array('ROLE_USER', 'ROLE_ADMIN'));
            }else{
                $user->setRoles(array('ROLE_USER'));
            }

            $this->setReference('user_'.$i , $user);

            $manager->persist($user);
            $manager->flush();
        }

    }

}