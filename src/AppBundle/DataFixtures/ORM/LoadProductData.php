<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Product;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadProductData extends AbstractFixture implements OrderedFixtureInterface
{
    public function getOrder()
    {
        return 200;
    }

    public function load(ObjectManager $manager)
    {

        for($i=1;  $i < 100; $i++){

            $product = new Product();

            $product->setTitle('Meiza M4'.$i)
                ->setDescription('Meiza M4 bla bla bla')
                ->setImage('1.png')
                ->setPrice(500)
                ->enable()
                ->setCategory($this->getReference('category_'.rand(1, 10)))
                ->setSku('01.001.Crap.m4'.$i);

            $this->setReference('product_'.$i , $product);

            $manager->persist($product);
            $manager->flush();
        }

    }

}