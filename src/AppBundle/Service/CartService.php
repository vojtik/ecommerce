<?php

namespace AppBundle\Service;

use AppBundle\AppEvents;
use AppBundle\Entity\Cart;
use AppBundle\Event\AdditionalPaymentEvent;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Psr\Log\LoggerAwareInterface;

/**
 * CartService
 */
class CartService implements LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * @var Registry
     */
    private $doctrine;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var EventDispatcher
     */
    private $eventDispatcher;

    public function __construct(Registry $doctrine, EventDispatcherInterface $eventDispatcher)
    {
        $this->doctrine = $doctrine;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function getCart(){

        $token = $this->tokenStorage->getToken();


        if(!is_null($token)){
            $user = $token->getUser();
        }else{
            throw new UnauthorizedHttpException();
        }

        if (is_null($user->getCart())){

            $cart = new Cart();

            $user->setCart($cart);
            $this->logger->debug('Created Cart for user '.$user->getId());

            $this->doctrine->persist($cart);
//            $this->doctrine->flush();
        }

        $this->eventDispatcher->dispatch(AppEvents::ADDITIONAL_PAYMENT_EVENT, new AdditionalPaymentEvent($user->getCart()));

        return $user->getCart();
    }

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function setTokenStorage($tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }


}