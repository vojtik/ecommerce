<?php

namespace AppBundle\Event;

use AppBundle\Entity\Cart;
use Symfony\Component\EventDispatcher\Event;


class AdditionalPaymentEvent extends Event{

    private $cart;

    public function __construct(Cart $cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return mixed
     */
    public function getCart()
    {
        return $this->cart;
    }

}