<?php

namespace AppBundle\EventListener;


use AppBundle\AppEvents;
use AppBundle\Event\AdditionalPaymentEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class CartListener implements EventSubscriberInterface{

    /**
     * @var AuthorizationChecker
     */
    private $authorizationChecker;

    /**
     * @param AuthorizationChecker $authorizationChecker
     */
    public function setAuthorizationChecker($authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }


    public function roleConditions(AdditionalPaymentEvent $event)
    {
        if($this->authorizationChecker->isGranted('ROLE_ADMIN')){
            $cart = $event->getCart();
            $cart->setTaxCost(0);
        }

    }

    public function subTotalConditions(AdditionalPaymentEvent $event)
    {
        $cart = $event->getCart();

        if($cart->getSubTotal() > 100){

            $cart->setShippingCost(0);
            $cart->setTaxCost(0);

            $event->stopPropagation();
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            AppEvents::ADDITIONAL_PAYMENT_EVENT => [
                array('roleConditions', 100),
                array('subTotalConditions', 110)
            ]
        ];
    }

}