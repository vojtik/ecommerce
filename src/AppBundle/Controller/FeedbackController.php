<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Feedback;
use AppBundle\Form\FeedbackType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route("/")
 */
class FeedbackController extends Controller
{
    /**
     * @Route("/feedback.html", name="feedback")
     * @Template()
     */
    public function indexAction(Request $request)
    {

        $form = $this->createForm(new FeedbackType(), new Feedback());

        $form->add('submit', 'submit');

        return ['form'=>$form->createView()];

//        // replace this example code with whatever you need
//        return $this->render('AppBundle:Feedback:index');
    }

    /**
     * @Route("/create_feedback.html", name="create_feedback",methods={"get","post"})
     * @Template("AppBundle:feedback:create.html.twig")
     */
    public function createAction(Request $request)
    {
        $form = $this->createFeedbackForm();
        $form->handleRequest($request);
        if($request->isMethod(Request::METHOD_POST) && $form->isValid() ) {
            $doctrine = $this->container->get('doctrine');
            $em = $doctrine->getEntityManager();
            $entity = $form->getData();
            $em->persist($entity);
            $em->flush();
            $this->get('session')->getFlashBag()->add('message','Your feedback is not important for us. We don\'t respond you soon.');
            return new RedirectResponse($this->generateUrl('feedback_message'));
//            return new RedirectResponse($this->generateUrl('feedback_message'));
        }
        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @return \Symfony\Component\Form\Form
     */
    protected function createFeedbackForm( Feedback $entity = null)
    {
        if(is_null($entity)) {
            $entity = new Feedback();
        }
        $form = $this->createForm(new FeedbackType(), $entity);
        $form->add('submit', 'submit');

        return $form;
    }

    /**
     * @Route("/success.html", name="feedback_message")
     * @Template("AppBundle:feedback:success.html.twig")
     */
    public function successAction()
    {
        return [
            'message' => 'Your feedback is not important for us. We don\'t respond you soon.'
        ];
    }
}