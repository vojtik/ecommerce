<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/category")
 */
class CategoryController extends Controller
{
    /**
     * @Route("/{category}.html", name="show_category")
     * @Template()
     */
    public function indexAction(Request $request, \AppBundle\Entity\Category $category)
    {

        $doctrine = $this->container->get('doctrine');
//        $categoryRepo = $doctrine->getRepository('AppBundle:Category');
//
//        $category = $categoryRepo->findCategoryById($id);

        $limit = $request->get('limit', 20);

        $productRepo = $doctrine->getRepository('AppBundle:Product');
        $products = $productRepo->findProductsByCategory($category, true, $limit);


        return ['products'=> $products];

//        // replace this example code with whatever you need
//        return $this->render('AppBundle:Category:index');
    }
}
