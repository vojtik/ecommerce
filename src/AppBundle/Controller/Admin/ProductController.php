<?php

namespace AppBundle\Controller\Admin;

use AppBundle\AppBundle;
use AppBundle\Entity\Product;
use AppBundle\Form\Admin\ProductType as AdminProductType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Route("/admin")
 */
class ProductController extends Controller
{
    /**
     * @Route("/index", name="admin_index")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $doctrine = $this->container->get('doctrine');

        $productRepo = $doctrine->getRepository('AppBundle:Product');
        $products = $productRepo->findAll();


        return ['products'=> $products];
    }

    /**
     * @Route("/create", name="admin_create")
     * @Template()
     */
    public function createAction(Request $request)
    {

        $product = new Product();

        $form = $this->createForm(new AdminProductType(), $product);
        $form->add('submit', 'submit');

        $form->handleRequest($request);

        if($request->isMethod(Request::METHOD_POST) && $form->isValid() ) {

            $doctrine = $this->container->get('doctrine');
            $em = $doctrine->getEntityManager();

            $product = $form->getData();
            $product->enable();

            $em->persist($product);
            $em->flush();

            $this->get('session')->getFlashBag()->add('message','Your feedback is not important for us. We don\'t respond you soon.');

            return new RedirectResponse($this->generateUrl('admin_index'));

        }
        return [
            'form' => $form->createView()
        ];
    }

    /**
     * @Route("/update-{id}", name="admin_update")
     * @Template()
     */
    public function updateAction(Request $request, $id)
    {
        $doctrine = $this->container->get('doctrine');
        $productRepo = $doctrine->getRepository('AppBundle:Product');

        $product = $productRepo->find($id);

        $form = $this->createForm(new AdminProductType(), $product);
        $form->add('submit', 'submit');

        $form->handleRequest($request);

        if($request->isMethod(Request::METHOD_POST) && $form->isValid() ) {

            $doctrine = $this->container->get('doctrine');
            $em = $doctrine->getEntityManager();

            $product = $form->getData();
            $product->enable();

            $em->persist($product);
            $em->flush();

            $this->get('session')->getFlashBag()->add('message','Your feedback is not important for us. We don\'t respond you soon.');

            return new RedirectResponse($this->generateUrl('admin_index'));

        }
        return [
            'form' => $form->createView()
        ];
    }


}