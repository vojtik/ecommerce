<?php

namespace AppBundle\Controller;

use AppBundle\AppBundle;
use AppBundle\Entity\Cart;
use AppBundle\Entity\CartItem;
use AppBundle\Entity\Feedback;
use AppBundle\Entity\Product;
use AppBundle\Form\FeedbackType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * @Route("/admin/crt")
 */
class CartController extends Controller
{
    /**
     * @Route("/show", name="show_cart")
     */
    public function showCartAction()
    {
        $this->container->get('app.cart_service')->getCart();
        $form = $this->createForm(new FeedbackType(), new Feedback());

        $form->add('submit', 'submit');
        return $this->render('AppBundle:FeedBack:index.html.twig',array('form'=>$form->createView(), 'cart' => $this->getUser()->getCart()));
    }

    /**
     * @Route("/add/{product}")
     */
    public function addCartAction(Product $product)
    {
        $em = $this->container->get('doctrine')->getEntityManager();
        $cart = $this->getUser()->getCart();

        $cartProduct = new CartItem();
        $cartProduct->setProduct($product);
        $cartProduct->setPrice(1499);
        $cartProduct->setCart($cart);

        $cart->setSubTotal(0);

        $em->persist($cartProduct);
//        $em->persist($cart);

        $em->flush();

        return new RedirectResponse($this->generateUrl('show_cart'));
    }
}